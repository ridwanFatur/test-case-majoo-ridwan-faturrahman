import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<List> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response =
          await dio.get("auto-complete", queryParameters: {
        "q": "movie",
      });

      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return [movieResponse, ""];
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout) {
        return [null, "Connection  Timeout Exception"];
      } else if (error.type == DioErrorType.receiveTimeout) {
        return [null, "Tidak terkoneksi ke Internet"];
      } else {
        return [null, "Terjadi Kesalahan"];
      }
    } on SocketException {
      return [null, "Tidak terkoneksi ke Internet"];
    } catch (e) {
      return [null, "Terjadi Kesalahan"];
    }
  }
}
