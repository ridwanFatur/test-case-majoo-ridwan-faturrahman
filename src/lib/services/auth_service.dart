import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/user_db.dart';

class AuthService {
  Future<User?> loginUser(User user) async {
    List<User> listUser = await UserDBProvider.db
        .getUser(email: user.email, password: user.password);
    if (listUser.length > 0) {
      return listUser[0];
    } else {
      return null;
    }
  }

  Future<bool> registerUser(User user) async {
    List<User> listUser = await UserDBProvider.db.getUser(email: user.email);

    if (listUser.length > 0) {
      return false;
    } else {
      await UserDBProvider.db.insertUserData(user);
      return true;
    }
  }
}
