import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;

Future<void> createInstance() async {
  var options = BaseOptions(
      baseUrl: "https://imdb8.p.rapidapi.com/",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "52669ae88cmsh986f1c474753f27p1cb4d2jsn62599f473c5e",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });

  dioInstance = Dio(options);
  dioInstance!.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  // dioInstance!.options.baseUrl =
  //     "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr";
  return dioInstance!;
}
