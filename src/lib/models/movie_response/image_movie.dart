class ImageMovie {
  ImageMovie({
    this.height,
    required this.imageUrl,
    this.width,
  });

  int? height;
  String imageUrl;
  int? width;

  factory ImageMovie.fromJson(Map<String, dynamic> json) {
    return ImageMovie(
      height: json["height"],
      imageUrl: json["imageUrl"],
      width: json["width"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "height": height,
      "imageUrl": imageUrl,
      "width": width,
    };
  }
}
