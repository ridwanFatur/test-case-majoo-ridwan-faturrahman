import 'package:majootestcase/models/movie_response/image_movie.dart';
import 'package:majootestcase/models/movie_response/series_movie.dart';

class Data {
  Data({
    required this.imageMovie,
    required this.id,   
    required this.title,
    this.s,
    this.q,
    this.rank,
    this.series,
    this.vt,
    this.year,
    this.yr,
  });

  ImageMovie imageMovie;
  String id;
  String title;
  String? s;
  String? q;
  int? rank; 
  List<Series>? series;
  int? vt;
  int? year;
  String? yr;

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      imageMovie: ImageMovie.fromJson(json["i"]),
      id: json["id"],
      title: json["l"],
      q: json["q"],
      rank: json["rank"],
      s: json["s"],
      series: json["v"] == null
          ? null
          : List<Series>.from(json["v"].map((x) => Series.fromJson(x))),
      vt: json["vt"],
      year: json["y"],
      yr: json["yr"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "i": imageMovie.toJson(),
      "id": id,
      "l": title,
      "q": q,
      "rank": rank,
      "s": s,
      "v": List<dynamic>.from(series!.map((x) => x.toJson())),
      "vt": vt,
      "y": year,
      "yr": yr,
    };
  }
}
