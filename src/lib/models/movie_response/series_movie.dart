import 'package:majootestcase/models/movie_response/image_movie.dart';

class Series {
  Series({
    required this.imageMovie,
    required this.id,
    required this.title,
    this.s,
  });

  ImageMovie imageMovie;
  String id;
  String title;
  String? s;

  factory Series.fromJson(Map<String, dynamic> json) {
    return Series(
      imageMovie: ImageMovie.fromJson(json["i"]),
      id: json["id"],
      title: json["l"],
      s: json["s"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "i": imageMovie.toJson(),
      "id": id,
      "l": title,
      "s": s,
    };
  }
}
