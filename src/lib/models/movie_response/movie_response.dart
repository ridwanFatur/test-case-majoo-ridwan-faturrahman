import 'package:majootestcase/models/movie_response/data_movie.dart';

class MovieResponse {
  MovieResponse({
    required this.data,
    required this.query,
    required this.v,
  });

  List<Data> data;
  String query;
  int v;

  factory MovieResponse.fromJson(Map<String, dynamic> json) {
    return MovieResponse(
      data: List<Data>.from(json["d"].map((x) {
        return Data.fromJson(x);
      })),
      query: json["q"],
      v: json["v"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "d": List<dynamic>.from(data.map((x) => x.toJson())),
      "q": query,
      "v": v,
    };
  }
}
