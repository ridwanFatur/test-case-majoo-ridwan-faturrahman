import 'dart:convert';

class User {
  String email;
  String? userName;
  String password;

  User({
    required this.email,
    this.userName,
    required this.password,
  });

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      email: map['email'],
      password: map['password'],
      userName: map['username'],
    );
  }

  Map<String, dynamic> toMap() {
    return {'email': email, 'password': password, 'username': userName};
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));
}
