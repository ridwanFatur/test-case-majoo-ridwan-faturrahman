import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserDBProvider {
  static final UserDBProvider db = UserDBProvider();
  Database? _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    }
    _database = await openDb();
    return _database!;
  }

  Future openDb() async {
    return await openDatabase(join(await getDatabasesPath(), "my_user.db"),
        version: 1,
        onOpen: (db) async {}, onCreate: (Database db, int version) async {
      await db.execute(
          "CREATE TABLE user(id INTEGER PRIMARY KEY autoincrement,username TEXT,email TEXT,password TEXT)");
    });
  }

  Future insertUserData(User model) async {
    final db = await database;
    return db.insert('user', model.toMap());
  }

  Future<List<User>> getUser({
    required String email,
    String? password,
  }) async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('user');
    List<User> list =
        maps.isNotEmpty ? maps.map((note) => User.fromMap(note)).toList() : [];
    list = list.where((element) {
      if (password == null) {
        return email == element.email;
      } else {
        return email == element.email && password == element.password;
      }
    }).toList();

    return list;
  }
}
