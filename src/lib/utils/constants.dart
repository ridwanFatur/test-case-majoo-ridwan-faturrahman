class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class SharedPreferenceKeyConstants{
  static const String USER_KEY = "user_value";
  static const String LOGGED_IN_KEY = "is_logged_in";
}
