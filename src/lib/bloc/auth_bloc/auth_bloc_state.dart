part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {
  final String email;
  final String password;

  AuthBlocLoginState({this.email = "", this.password = ""});

  @override
  List<Object> get props => [email, password];
}

class AuthBlocSuccesState extends AuthBlocState {}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
