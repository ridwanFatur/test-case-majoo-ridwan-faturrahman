import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:majootestcase/common/widget/custom_dialog_progress.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/auth_service.dart';
import 'package:majootestcase/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn =
        sharedPreferences.getBool(SharedPreferenceKeyConstants.LOGGED_IN_KEY);
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void logoutUser() async {
    emit(AuthBlocLoadingState());
    await Future.delayed(Duration(seconds: 1));
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool(
        SharedPreferenceKeyConstants.LOGGED_IN_KEY, false);
    await sharedPreferences.remove(SharedPreferenceKeyConstants.USER_KEY);
    emit(AuthBlocLoginState());
  }

  void registerUser(
    User user,
    BuildContext context, {
    required VoidCallback onSuccess,
    required VoidCallback onFailed,
  }) async {
    var response = await showCustomDialogProgress(context, (context) async {
      await Future.delayed(Duration(seconds: 1));
      bool isSuccessRegister = await AuthService().registerUser(user);
      Navigator.of(context).pop(isSuccessRegister);
    });

    if (response != null && response) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      await sharedPreferences.setBool(
          SharedPreferenceKeyConstants.LOGGED_IN_KEY, true);
      String data = user.toJson();
      await sharedPreferences.setString(
          SharedPreferenceKeyConstants.USER_KEY, data);
      onSuccess();
      Navigator.of(context).pop();
      emit(AuthBlocLoggedInState());
    } else {
       onFailed();
    }
  }

  void loginUser(
    User user, {
    required VoidCallback onSuccess,
    required VoidCallback onFailed,
  }) async {
    emit(AuthBlocLoadingState());
    await Future.delayed(Duration(seconds: 1));
    User? userLogin = await AuthService().loginUser(user);
    if (userLogin != null) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      await sharedPreferences.setBool(
          SharedPreferenceKeyConstants.LOGGED_IN_KEY, true);
      String data = userLogin.toJson();
      await sharedPreferences.setString(
          SharedPreferenceKeyConstants.USER_KEY, data);
      onSuccess();
      emit(AuthBlocLoggedInState());
    } else {
      onFailed();
      emit(AuthBlocLoginState(
        email: user.email,
        password: user.password,
      ));
    }
  }
}
