import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response/data_movie.dart';
import 'package:majootestcase/models/movie_response/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    ApiServices apiServices = ApiServices();
    List response = await apiServices.getMovieList();
    MovieResponse? movieResponse = response[0];
    String message = response[1];

    if (movieResponse == null) {
      emit(HomeBlocErrorState(message));
    } else {
      emit(HomeBlocLoadedState(movieResponse.data));
    }
  }
}
