import 'package:flutter/material.dart';

Future showCustomDialogProgress(BuildContext context, Function function) {
  return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: CustomDialogProgress(function: function));
    },
  );
}

class CustomDialogProgress extends StatefulWidget {
  CustomDialogProgress({required this.function});
  final Function function;

  @override
  State<CustomDialogProgress> createState() => _CustomDialogProgress();
}

class _CustomDialogProgress extends State<CustomDialogProgress> {
  @override
  void initState() {
    super.initState();
    widget.function(context);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        child: Center(
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 40),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Loading")
                ],
              )),
        ),
      ),
    );
  }
}
