import 'package:flutter/material.dart';

Future showCustomDialogConfirmation(
  BuildContext context, {
  required String confirmationQuestion,
  required String negativeCaption,
  required String positiveCaption,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: CustomDialogConfirmation(
          confirmationQuestion: confirmationQuestion,
          negativeCaption: negativeCaption,
          positiveCaption: positiveCaption,
        ),
      );
    },
  );
}

class CustomDialogConfirmation extends StatelessWidget {
  final String confirmationQuestion;
  final String negativeCaption;
  final String positiveCaption;

  CustomDialogConfirmation({
    required this.confirmationQuestion,
    required this.negativeCaption,
    required this.positiveCaption,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 14),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                confirmationQuestion,
                style: TextStyle(
                  fontSize: 19,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(width: 15),
                  Expanded(
                    child: buttonSection(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      title: negativeCaption,
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: buttonSection(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      title: positiveCaption,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buttonSection({required String title, required VoidCallback onPressed}) {
    return Material(
      color: Colors.blue,
      borderRadius: BorderRadius.circular(10),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        splashFactory: InkRipple.splashFactory,
        onTap: () {
          onPressed();
        },
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(14),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 14,
              color: Colors.white,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
