import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:majootestcase/utils/show_toast.dart';

class LoginPage extends StatefulWidget {
  final String initialEmail;
  final String initialPassword;
  LoginPage({required this.initialEmail, required this.initialPassword});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  late final _emailController;
  late final _passwordController;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  String errorMessage = "";
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
    _emailController = TextController(initialValue: widget.initialEmail);
    _passwordController = TextController(initialValue: widget.initialPassword);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Selamat Datang',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'Silahkan login terlebih dahulu',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 9,
              ),
              _form(),
              SizedBox(
                height: 50,
              ),
              CustomButton(
                text: 'Login',
                onPressed: handleLogin,
                height: 100,
              ),
              SizedBox(
                height: 20,
              ),
              _register(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  setErrorMessage(
                      "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                  return "E-mail tidak boleh kosong";
                }

                final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (!pattern.hasMatch(val)) {
                  setErrorMessage("Masukkan e-mail yang valid");
                  return 'E-mail tidak valid';
                }
              }
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  setErrorMessage(
                      "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                  return "Password tidak boleh kosong";
                }
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          var authCubit = context.read<AuthBlocCubit>();
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return BlocProvider.value(
              value: authCubit,
              child: RegisterPage(),
            );
          }));
        },
        child: RichText(
          text: TextSpan(
            text: 'Belum punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(
                text: 'Daftar',
              ),
            ],
          ),
        ),
      ),
    );
  }

  void setErrorMessage(String message) {
    if (errorMessage.isEmpty) {
      errorMessage = message;
    }
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    errorMessage = "";
    if (formKey.currentState!.validate() == true) {
      User user = User(
        email: _email,
        password: _password,
      );
      BlocProvider.of<AuthBlocCubit>(context).loginUser(
        user,
        onSuccess: () {
          showToast("Login Berhasil");
        },
        onFailed: () {
          showToast("Login gagal, periksa kembali inputan anda");
        },
      );
    } else {
      showToast(errorMessage);
    }
  }
}
