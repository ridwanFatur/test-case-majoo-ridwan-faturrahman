import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/show_toast.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late final _emailController;
  late final _usernameController;
  late final _passwordController;

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  String errorMessage = "";
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
    _emailController = TextController(initialValue: "");
    _passwordController = TextController(initialValue: "");
    _usernameController = TextController(initialValue: "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Daftar sekarang',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'Silahkan isi form di bawah ini',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 9,
              ),
              _form(),
              SizedBox(
                height: 50,
              ),
              CustomButton(
                text: 'Register',
                onPressed: handleRegister,
                height: 100,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Example: RidwanF',
            label: 'Username',
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  setErrorMessage(
                      "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                  return "Username tidak boleh kosong";
                }
              }
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  setErrorMessage(
                      "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                  return "E-mail tidak boleh kosong";
                }

                final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (!pattern.hasMatch(val)) {
                  setErrorMessage("Masukkan e-mail yang valid");
                  return 'E-mail tidak valid';
                }
              }
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  setErrorMessage(
                      "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                  return "Password tidak boleh kosong";
                }
              }
            },
          ),
        ],
      ),
    );
  }

  void setErrorMessage(String message) {
    if (errorMessage.isEmpty) {
      errorMessage = message;
    }
  }

  void handleRegister() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _username = _usernameController.value;

    errorMessage = "";
    if (formKey.currentState!.validate() == true) {
      User user = User(
        email: _email,
        password: _password,
        userName: _username,
      );

      BlocProvider.of<AuthBlocCubit>(context).registerUser(
        user,
        context,
        onSuccess: () {
          showToast("Register Berhasil");
        },
        onFailed: () {
          showToast("Register gagal, gunakan email yang lain");
        },
      );
    } else {
      showToast(errorMessage);
    }
  }
}
