import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response/data_movie.dart';
import 'package:majootestcase/models/movie_response/series_movie.dart';

class DetailPage extends StatefulWidget {
  final Data data;
  DetailPage({required this.data});

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Hero(
                    tag: widget.data.id,
                    child: Image.network(
                      widget.data.imageMovie.imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Text(
                  widget.data.title.toUpperCase(),
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Year",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black87,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text(
                        widget.data.year != null
                            ? widget.data.year.toString()
                            : "-",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.all(15),
                child: Text(
                  "Series",
                  style: TextStyle(
                    fontSize: 17,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(height: 10),
              listSeries(),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  Widget listSeries() {
    if (widget.data.series != null && widget.data.series!.length > 0) {
      return ListView.separated(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return seriesWidget(widget.data.series![index]);
        },
        separatorBuilder: (context, index) {
          return SizedBox(height: 20);
        },
        itemCount: widget.data.series!.length,
      );
    } else {
      return Text(
        "No Series",
        style: TextStyle(
          fontSize: 14,
          color: Colors.grey,
        ),
      );
    }
  }

  Widget seriesWidget(Series series) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Material(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(25.0),
          ),
        ),
        elevation: 10,
        clipBehavior: Clip.antiAlias,
        child: Container(
          child: Stack(
            children: [
              Column(
                children: [
                  Container(
                    child: Image.network(
                      series.imageMovie.imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: Text(
                      series.title,
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              ),
              Positioned.fill(
                child: Material(
                  color: Colors.white.withOpacity(0.1),
                  child: InkWell(
                    splashFactory: InkRipple.splashFactory,
                    splashColor: Colors.blue.withOpacity(0.2),
                    onTap: () {},
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
