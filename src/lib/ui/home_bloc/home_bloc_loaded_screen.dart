import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response/data_movie.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<Data> data;
  const HomeBlocLoadedScreen({required this.data});

  @override
  State<HomeBlocLoadedScreen> createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GridView.count(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          crossAxisCount: 2,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          children: List.generate(
            widget.data.length,
            (index) {
              return movieItemWidget(widget.data[index]);
            },
          ),
        ),
      ),
    );
  }

  Widget movieItemWidget(Data data) {
    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(25.0),
        ),
      ),
      elevation: 10,
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: [
          Column(
            children: [
              Expanded(
                child: Container(
                  width: double.infinity,
                  child: Hero(
                    tag: data.id,
                    child: Image.network(
                      data.imageMovie.imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text(data.title, textDirection: TextDirection.ltr),
              )
            ],
          ),
          Positioned.fill(
            child: Material(
              color: Colors.white.withOpacity(0.1),
              child: InkWell(
                splashFactory: InkRipple.splashFactory,
                splashColor: Colors.blue.withOpacity(0.2),
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    return DetailPage(data: data);
                  }));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
