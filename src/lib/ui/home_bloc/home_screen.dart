import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/home_bloc/account_page.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedPage = 0;
  @override
  Widget build(BuildContext context) {
    var homeBlocCubit = context.read<HomeBlocCubit>();

    return Scaffold(
      body: IndexedStack(
        index: _selectedPage,
        children: [
          BlocProvider.value(
            value: homeBlocCubit,
            child: HomeBlocScreen(),
          ),
          AccountPage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.movie), label: "Movies"),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined), label: "Account"),
        ],
        currentIndex: _selectedPage,
        onTap: (int index) {
          setState(() {
            _selectedPage = index;
          });
        },
      ),
    );
  }
}
