import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_dialog_confirmation.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountPage extends StatefulWidget {
  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  String username = "";
  String email = "";

  @override
  void initState() {
    super.initState();
    getProfile();
  }

  void getProfile() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? data =
        sharedPreferences.getString(SharedPreferenceKeyConstants.USER_KEY);
    print(data);
    if (data != null) {
      User user = User.fromJson(data);
      setState(() {
        username = user.userName!;
        email = user.email;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                username,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Text(
                email,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
              SizedBox(height: 40),
              Material(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(10),
                clipBehavior: Clip.antiAlias,
                child: InkWell(
                  splashFactory: InkRipple.splashFactory,
                  onTap: () async {
                    bool? isLogout = await showCustomDialogConfirmation(
                      context,
                      confirmationQuestion: "Kamu yakin keluar?",
                      negativeCaption: "Tidak",
                      positiveCaption: "Iya",
                    );

                    if (isLogout != null && isLogout) {
                      BlocProvider.of<AuthBlocCubit>(context).logoutUser();
                    }
                  },
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(20),
                    child: Text(
                      "Log Out",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
