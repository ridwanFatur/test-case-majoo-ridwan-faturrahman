import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_loaded_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      } else if (state is HomeBlocLoadingState) {
        return LoadingIndicator(
          color: Colors.blue,
        );
      } else if (state is HomeBlocInitialState) {
        return LoadingIndicator(
          color: Colors.blue,
        );
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(
          message: state.error,
          retry: () {
            BlocProvider.of<HomeBlocCubit>(context).fetchingData();
          },
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}
