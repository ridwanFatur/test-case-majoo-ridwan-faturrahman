import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final double height;
  final Color color;

  const LoadingIndicator({
    this.height = 10,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(strokeWidth: 100),
      ),
    );
  }
}
